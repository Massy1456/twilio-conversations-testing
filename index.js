require('dotenv').config()
const accountSid = process.env.TWILIO_ACCOUNT_SID
const authToken = process.env.TWILIO_AUTH_TOKEN
const convoSid = process.env.CONVO_ID
const myNumber = '+16822391122'
const twilioNumber = '+19892997259'

const client = require('twilio')(accountSid, authToken);

//* Create a conversation named 'My First Conversation'
// client.conversations.conversations
//                     .create({friendlyName: 'My First Conversation'})
//                     .then(conversation => console.log(conversation.sid));

//* Fetching a conversation
//* Retreive the new conversation to attain the Chat Service SID
client.conversations.conversations(convoSid)
      .fetch()
      .then(conversation => console.log(conversation.chatServiceSid));

//* Adding a conversation participant (SMS)
// client.conversations.conversations(convoSid)
//   .participants
//   .create({
//      'messagingBinding.address': myNumber,
//      'messagingBinding.proxyAddress': twilioNumber
//    })
//   .then(participant => console.log(participant.sid));

//* Adding a conversation participant (Chat)
// client.conversations.conversations(convoSid)
//                     .participants
//                     .create({identity: 'testPineapple'})
//                     .then(participant => console.log(participant.sid));


//* Update conversation metadata
// client.conversations.conversations(convoSid)
//       .update({friendlyName: 'Important Customer Question'})
//       .then(conversation => console.log(conversation.friendlyName));

//* Delete a conversation
// client.conversations.conversations(convoSid)
//                     .remove();
